# Pour l'instant je n'utilise pas ce fichier, toute les infos sont dans gestion_lab.py

def menu():
    ''' initialise une liste qui va stocker le menu 
    in : m = []
    '''
    return []

def ajouter_entree(intitule, commande):
    m = menu()
    m.append((intitule, commande))

def afficher_menu(m):
    for num, (intitule, _) in enumerate(m, 1):
        print (num, intitule)
    print (0, 'Quitter')

def input_choix(m, choix):
    pass # demande un entier et virifie s'il est bon. verification assert <= 0 <= len(menu) 
    choix = input(f"Que voulez vous faire ? Entrez un numéro entre 0 et {len(menu)}")
    return choix

def traiter_choix(m):
    assert 0<= choix <= len(menu)
    if choix != 0:
        m[choix-1][1]()
        ''' la meme chose en plus explicite
        entree = m[choix - 1]
        cmd = entree[1]
        cmd()
        '''
        #ici il faut mettre une exception au cas ou  le choix n'est pas bon

def gerer_menu (m):
    # gerer l'excecution du menu, 