from labo import *

def main():
    l = {}
    # assert taille(l) == 0
    choix = None
    while choix != 0:
        afficher_menu()
        choix = input_choix()
        traiter_choix(l, choix)

def input_choix():
    choix = -1
    while choix not in (1,2):
        try:
            choix = int(input("Saisir choix : "))
            return choix
        except ValueError:
            pass

def afficher_menu():
    print("0 - Sortir")
    print("1 - Enregistrer Entrée")
    print("2 - Enregistrer Départ")
    print("3 - Changer de bureau")
    print("4 - Changer de nom")
    print("5 - Chercher un membre du laboratoire")
    print("6 - Retrouver le n° de bureau d'un membre")
    print("7 - Affichage du listing du personnel")
    print("8 - Affichage des membres par bureau")
    print("9 - Affichage des membres par ordre lexicographique")

def traiter_choix(l, choix):
    if choix == 1:
        nom = input("Nom : ")
        bureau = input("Bureau : ")
        enregistrer_arrivee(l, nom, bureau)
    elif choix == 2:
        nom = input("Nom : ")
        enregistrer_depart(l, nom)
    elif choix == 3:
        nom = input("Nom : ")
        bureau = input("Nouveau bureau : ")
        changer_bureau(l, nom, bureau)
    elif choix == 4:
        nom = input("Nom à modifier : ")
        new_nom = input("Nouveau nom : ")
        changer_nom(l, nom, new_nom)
    elif choix == 5:
        nom = input("Nom à trouver : ")
        trouver_nom(l, nom)
    elif choix == 6:
        nom = input("Nom : ")
        trouver_bureau(l, nom)
    elif choix == 7:
        listing(l)
    elif choix == 8:
        par_bureau(l)
    elif choix == 9:
        par_ordre(l)


main()













