# arrivée, bureau, présence
# # labo() ->> renvoie

# irit = labo()
# enregistrer_arrivee("irit","David","F301")


# enregistrer_depart(labo, nom)
# assert False
# except AbsentException
#     pass
import json, webbrowser


class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass


def labo():
    return {labo}


def taille(labo):
    return len(labo)


def enregistrer_arrivee(labo, nom, bureau):
    # y = json.loads(x)
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    if nom in labo:
        raise PresentException
    labo[nom] = bureau
    print("\n", f"{nom} est enregistré(e) au bureau n°{bureau}.", "\n")
    with open("personnel.json", "w") as outfile:
        json.dump(labo, outfile)


def enregistrer_depart(labo, nom):
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    if nom not in labo:
        raise AbsentException
    del labo[nom]
    print("\n", f"{nom} est désormais supprimé(e) du personnel.", "\n")
    with open("personnel.json", "w") as outfile:
        json.dump(labo, outfile)


def changer_bureau(labo, nom, bureau):
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    if nom not in labo:
        raise AbsentException
    old_bureau = labo[nom]
    labo[nom] = bureau
    print(
        "\n", f"{nom} est passé(e) du bureau n°{old_bureau} au n°{bureau}.", "\n")
    with open("personnel.json", "w") as outfile:
        json.dump(labo, outfile)


def changer_nom(labo, nom, new_nom):
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    if nom not in labo:
        raise AbsentException
    labo[new_nom] = labo[nom]
    del labo[nom]
    print("\n", f"{nom} s'appelle désormais {new_nom}.", "\n")
    with open("personnel.json", "w") as outfile:
        json.dump(labo, outfile)


def trouver_nom(labo, nom):
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    if nom not in labo:
        print("\n", f"{nom} n'est pas membre du laboratoire.", "\n")
    else:
        print("\n", f"{nom} est membre du laboratoire.", "\n")


def trouver_bureau(labo, nom):
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    if nom not in labo:
        raise AbsentException
    bureau = labo[nom]
    print("\n", f"{nom} est assis(e) bureau n°{bureau}", "\n")

def listing(labo):
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    [print(k, "n°", v) for k, v in sorted(labo.items(), key=lambda t: t[1])]

# def par_bureau(labo):
#     with open("personnel.json", "r") as outfile:
#         labo = json.load(outfile)
#     for bureau in sorted(set(labo.values())):
#         print(f"{bureau}:")
#         for key, value in labo.items():
#             if value == bureau:
#                 print(f"- {key}")

def par_bureau(labo):
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    with open('sortie.html', 'w') as myFile:
        myFile.write('<html>')
        myFile.write('<body style="padding-left: 45%; padding-top: 25px; background-color: rgb(43, 44, 44); color: ghostwhite;">')
        for bureau in sorted(set(labo.values())):
            myFile.write('<p>%s:</p>' % (bureau))
            for key, value in labo.items():
                if value == bureau:
                    myFile.write('<p style="margin-left: 15px;">> %s</p>' % (key))
        myFile.write('</body>')
        myFile.write('</html>')
        webbrowser.open('sortie.html')

def par_ordre(labo):
    with open("personnel.json", "r") as outfile:
        labo = json.load(outfile)
    with open('sortie.html', 'w') as myFile:
        myFile.write('<html>')
        myFile.write('<body style="padding-left: 45%; padding-top: 25px; background-color: rgb(43, 44, 44); color: ghostwhite;">')
        for key, value in sorted(labo.items()):
            myFile.write(f"<p>{key} > {value}</p>")
        myFile.write('</body>')
        myFile.write('</html>')
        webbrowser.open('sortie.html')

















